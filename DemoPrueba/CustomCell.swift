//
//  CustomCell.swift
//  DemoPrueba
//
//  Created by Procesos on 8/09/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    
    @IBOutlet weak var entityImage: ProfileImageView!
    
    @IBOutlet weak var entityName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
