
import Foundation
import CoreData
import UIKit

class DataManager {
    
    class func getFromFetchRequest(nome: String) -> (sucess:Bool, objects:NSArray){
        let delegate = (UIApplication.sharedApplication()).delegate as! AppDelegate
        let model:NSManagedObjectModel = delegate.managedObjectModel
        let fetchRequest = model.fetchRequestTemplateForName(nome)
        let context = delegate.managedObjectContext
      
        do {
            let results = try context.executeFetchRequest(fetchRequest!)
            return (true, results)
        } catch let error as NSError {
            print("Fetch failed: \(error.localizedDescription)")
            return (false, [])
        }
    }
    
    class func getFromFetchRequest(nome: String, variaveis:NSDictionary) -> (sucess:Bool, objects:NSArray){
        let delegate = (UIApplication.sharedApplication()).delegate as! AppDelegate
        let model:NSManagedObjectModel = delegate.managedObjectModel
        let fetchRequest = model.fetchRequestFromTemplateWithName(nome, substitutionVariables: variaveis as! [String : AnyObject])
        let context = delegate.managedObjectContext
        
        do {
            let results = try context.executeFetchRequest(fetchRequest!)
            return (true, results)
        } catch let error as NSError {
            print("Fetch failed: \(error.localizedDescription)")
            return (false, [])
        }
    }
    
    class func getEntity(entidade: String) -> (NSEntityDescription){
        let delegate = (UIApplication.sharedApplication()).delegate as! AppDelegate
        let context:NSManagedObjectContext? = delegate.managedObjectContext
        let description:NSEntityDescription = NSEntityDescription.entityForName(entidade, inManagedObjectContext: context!)!
        
        return description
    }
    
    class func getContext () -> (NSManagedObjectContext) {
        let delegate = (UIApplication.sharedApplication()).delegate as! AppDelegate
        return delegate.managedObjectContext
    }
    
    class func getAllManagedObjectsFromEntity(entity: NSEntityDescription) -> (sucess: Bool, objects: NSArray){
        
        let delegate = (UIApplication.sharedApplication()).delegate as! AppDelegate
        let context:NSManagedObjectContext? = delegate.managedObjectContext
        
        let request:NSFetchRequest = NSFetchRequest()
        request.entity = entity
        
        var error:NSError?
        var objects:NSArray?
        do {
            objects = try context?.executeFetchRequest(request)
        } catch let error1 as NSError {
            error = error1
            objects = nil
        }
        
        if(error == nil){
            return(true, objects!)
        }else{
            NSLog(error!.description)
            return(false, objects!)
        }
        
    }
    
    class func getManagedObjectsFromEntity(entity: NSEntityDescription, predicate:NSPredicate) -> (sucess: Bool, objects: NSArray){
        let delegate = (UIApplication.sharedApplication()).delegate as! AppDelegate
        let context:NSManagedObjectContext? = delegate.managedObjectContext
        
        let request:NSFetchRequest = NSFetchRequest()
        request.entity = entity
        request.predicate = predicate
        
        var error:NSError?
        var objects:NSArray?
        do {
            objects = try context?.executeFetchRequest(request)
        } catch let error1 as NSError {
            error = error1
            objects = nil
        }
        
        if(error == nil){
            return(true, objects!)
        }else{
            NSLog(error!.description)
            return(false, objects!)
        }
    }
    
}