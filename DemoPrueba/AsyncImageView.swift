//
//  AsyncImageView.swift
//  AC Custom Keyboard
//
//  Created by Luis on 5/04/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

import UIKit
import Foundation

class AsyncImageView: UIImageView  {
    
    
    var activityIndicatorStyle: UIActivityIndicatorViewStyle!
    var activityView: UIActivityIndicatorView!
    var data: NSData!
    var img: UIImage!
    
    let cache = NSCache()
    
    class var sharedLoader : AsyncImageView {
        struct Static {
            static let instance : AsyncImageView = AsyncImageView()
        }
        return Static.instance
    }
    
    func removeCacheData() {
        print("ELIMINADO IMAGENES DE CACHE")
        self.cache .removeAllObjects()
    }
    
    // MARK: - Save Image At Document Directory
    func saveImageDocumentDirectory(name: String, imageData: NSData){
        let nameImg = name.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let fileManager = NSFileManager.defaultManager()
        let paths = (NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString).stringByAppendingPathComponent(nameImg + ".png")
        //let image = UIImage(named: "apple.jpg")
        //let imageData = UIImageJPEGRepresentation(image!, 0.5)
        fileManager.createFileAtPath(paths as String, contents: imageData, attributes: nil)
    }
    
    // MARK: - Get Document Directory Path
    func getDocumentsDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    // MARK: - Get Image from Document Directory
    func getImage(name: String) -> UIImage? {
        let nameImg = name.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let fileManager = NSFileManager.defaultManager()
        let imagePAth = (self.getDocumentsDirectory() as NSString).stringByAppendingPathComponent(nameImg + ".png")
        if fileManager.fileExistsAtPath(imagePAth){
            //self.imageView.image = UIImage(contentsOfFile: imagePAth)
            return UIImage(contentsOfFile: imagePAth)!
        }else{
            print("No Image")
        }
        
        return nil
    }
    
    // MARK: - Create Directory
    func createDirectory(){
        let fileManager = NSFileManager.defaultManager()
        let paths = (NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString).stringByAppendingPathComponent("customDirectory")
        if !fileManager.fileExistsAtPath(paths){
            try! fileManager.createDirectoryAtPath(paths, withIntermediateDirectories: true, attributes: nil)
        }else{
            print("Already dictionary created.")
        }
    }
    
    // MARK: - Delete Directory
    func deleteDirectory(){
        let fileManager = NSFileManager.defaultManager()
        let paths = (NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString).stringByAppendingPathComponent("customDirectory")
        if fileManager.fileExistsAtPath(paths){
            try! fileManager.removeItemAtPath(paths)
        }else{
            print("Something wronge.")
        }
    }
    
}

extension String {
    
    func stringByAppendingPathComponent(path: String) -> String {
        
        let nsSt = self as NSString
        
        return nsSt.stringByAppendingPathComponent(path)
    }
}
