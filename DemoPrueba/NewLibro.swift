//
//  NewLibro.swift
//  DemoPrueba
//
//  Created by Luis on 13/09/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

enum LibroInteractionType {
    case saveInteraction
    case editInteraction
}

class NewLibro: UIViewController, UINavigationControllerDelegate {
    
    var currentAuthor : Author!
    private var longitud:NSInteger = 0
    var currentLibro : Libro!
    var IDUniqueAuthor: String?
    var takenImage: UIImage?
    var isImagenCamGal : Bool = true
    @IBOutlet weak var profileLibro: ProfileImageView!
    var interactionType: LibroInteractionType!
    @IBOutlet weak var txtURLImagen: UITextField!
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtYear: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        IDUniqueAuthor = currentAuthor .objectID.URIRepresentation().absoluteString.replace("/", withString: "")
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(NewLibro.choosePhotoSource))
        profileLibro.addGestureRecognizer(tapGestureRecognizer)
        
        addDoneKeyboard(txtYear)
        
        if interactionType == .saveInteraction {
            self.navigationItem.title = "Nuevo Libro"
        }
        else {
            self.navigationItem.title = "Editar Libro"
            
            txtNombre.text = currentLibro.name
            txtYear.text = currentLibro.year
            
            if currentLibro.urlImagen.characters.count > 0 {
                txtURLImagen.text = currentLibro.urlImagen
                profileLibro.sd_setImageWithURL(NSURL(string: txtURLImagen.text!), placeholderImage: UIImage(named: "defaultProfile"))
            }else {
                profileLibro.image = AsyncImageView.sharedLoader.getImage(currentLibro .objectID.URIRepresentation().absoluteString.replace("/", withString: "") + currentLibro.name)
            }
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func donwloadImage(sender: AnyObject) {
        
        if txtURLImagen.text!.isEmpty {
            showAlert("Ingrese una dirección URL")
            return
        }
        if let urlString = txtURLImagen.text {
            if let URL = NSURL(string: urlString) {
                isImagenCamGal = false
                profileLibro.sd_setImageWithURL(URL, placeholderImage: UIImage(named: "defaultProfile"))
            }else{
                showAlert("Ingrese una URL válida")
                return
            }
        }
    }
    
    @IBAction func onClickInput(sender: AnyObject) {
        
        if txtNombre.text!.isEmpty {
            showAlert("Ingrese un nombre de Libro")
            return
        }
        
        if txtYear.text!.isEmpty || txtYear.text?.characters.count < 4 {
            showAlert("Ingrese un año valido")
            return
        }
        
        if interactionType == .editInteraction {
            
            currentLibro.name = txtNombre.text!
            currentLibro.urlImagen = txtURLImagen.text!
            currentLibro.year = txtYear.text!
            
            if let data = UIImagePNGRepresentation(profileLibro.image!) {
                AsyncImageView.sharedLoader.saveImageDocumentDirectory(currentLibro .objectID.URIRepresentation().absoluteString.replace("/", withString: "") + currentLibro.name, imageData: data)
            }
            currentLibro.save()
            
        }else {
            
            if isImagenCamGal {
                txtURLImagen.text = ""
            }
            
            Libro.saveLibro(IDUniqueAuthor!, name: txtNombre.text!, urlImagen: txtURLImagen.text!, year: txtYear.text!, image: profileLibro.image!)
        }
        
        showAlert()
    }
    
    private func showAlert() {
        let alertController = UIAlertController(title: "Alerta", message: interactionType == .saveInteraction ? "Registro exitoso" : "Modificacion exitosa", preferredStyle: .Alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            
            self.navigationController?.popViewControllerAnimated(true)
            
        }
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true) {
            // ...
        }
    }
    
    func showAlert(msj: String) {
        let alertController = UIAlertController(title: "Demo", message: msj, preferredStyle: .Alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func doneButtonAction() {
        if txtYear.isFirstResponder() {
            txtYear.resignFirstResponder()
        }
    }
    
    func addDoneKeyboard(textfield:UITextField) {
        textfield.keyboardType = UIKeyboardType.NumberPad
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, self.view.frame.size.width, 50))
        doneToolbar.barStyle = .Default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector(NewLibro.doneButtonAction))
        
        var items: [UIBarButtonItem] = []
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        textfield.inputAccessoryView = doneToolbar
    }

}

//MARK: Photo Image
extension NewLibro {
    
    func choosePhotoSource() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let chooseFromLibrary = UIAlertAction(title: "Escoger de la fototeca", style: .Default)  { (action) in
            
            NSOperationQueue.mainQueue().addOperationWithBlock({
                self.initializeImagePickerController(.PhotoLibrary)
            })
        }
        
        alertController.addAction(chooseFromLibrary)
        
        
        let takePhoto = UIAlertAction(title: "Tomar con la cámara", style: .Default)  { (action) in
            
            NSOperationQueue.mainQueue().addOperationWithBlock({
                self.initializeImagePickerController(.Camera)
            })
        }
        
        alertController.addAction(takePhoto)
        
        let cancel = UIAlertAction(title: "Cancelar", style: .Cancel, handler:nil)
        alertController.addAction(cancel)
        
        self.presentViewController(alertController, animated:true, completion: nil)
    }
    
    func initializeImagePickerController(sourceType: UIImagePickerControllerSourceType) {
        
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        picker.sourceType = sourceType
        presentViewController(picker, animated: true, completion: nil)
    }
    
}

extension NewLibro: UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        var newImage: UIImage
        
        if let possibleImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            newImage = possibleImage
        }
            
        else if let possibleImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            newImage = possibleImage
        }
            
        else {
            return
        }
        
        takenImage = newImage
        profileLibro.image = takenImage
        txtURLImagen.text = ""
        isImagenCamGal = true
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}


extension NewLibro : UITextFieldDelegate {
    
    func animateTextField(textField: UITextField, up: Bool, withOffset offset:CGFloat)
    {
        let movementDistance : Int = -Int(offset)
        let movementDuration : Double = 0.4
        let movement : Int = (up ? movementDistance : -movementDistance)
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = CGRectOffset(self.view.frame, 0, CGFloat(movement))
        UIView.commitAnimations()
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        self.animateTextField(textField, up: true, withOffset: textField.frame.origin.y / 2)
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        self.animateTextField(textField, up: false, withOffset: textField.frame.origin.y / 2)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- UITextFieldDelegate
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        longitud = textField.tag == 0 ? 100 : (textField.tag == 1 ? 30 : 4)
        
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        
        return newLength <= longitud
    }
}
