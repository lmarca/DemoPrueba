//
//  ViewController.swift
//  DemoPrueba
//
//  Created by Procesos on 8/09/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var searchActive : Bool = false
    var authorDataSource = [Author]()
    var filtered = [Author]()
    var currentAuthor : Author!
    
    override func viewWillAppear(animated: Bool) {
        
        let persona = DataManager.getAllManagedObjectsFromEntity(Author.entityDescription())
        authorDataSource.removeAll()
        if persona.sucess {
            for author in persona.objects as! [Author]{
                authorDataSource.append(author)
            }
        }
        self.tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Mis autores"
        
        self.automaticallyAdjustsScrollViewInsets = true
        self.edgesForExtendedLayout = .None

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ViewController: UISearchBarDelegate {

    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {

        if (searchText.characters.count == 0) {
            searchBar.resignFirstResponder()
        }

        let predicate = NSPredicate(format: "name CONTAINS[cd] %@", searchText.lowercaseString)
        
        let result = DataManager.getManagedObjectsFromEntity(Author.entityDescription(), predicate: predicate)
        if (result.sucess){
            if result.objects.count > 0 {
                filtered.removeAll()
                for author in result.objects as! [Author]{
                    if !filtered.contains(author) {
                        filtered .append(author)
                    }
                }
            }else{
                filtered.removeAll()
            }
        }
        
        if(filtered.count == 0){
            searchActive = false
        } else {
            searchActive = true
        }
        self.tableView.reloadData()
    }
}

extension ViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return filtered.count
        }
        return authorDataSource.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if authorDataSource.count > 0
        {
            tableView.separatorStyle = .SingleLine
            numOfSections                = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height))
            noDataLabel.text             = "No data available"
            noDataLabel.textColor        = UIColor.blackColor()
            noDataLabel.textAlignment    = .Center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .None
        }
        return numOfSections
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CustomCell", forIndexPath: indexPath) as! CustomCell
      
        if(searchActive){
            currentAuthor = filtered[indexPath.row]
        }else {
            currentAuthor = authorDataSource[indexPath.row]
        }
        
        cell.entityName.text = currentAuthor.name
        
        let moID = currentAuthor .objectID.URIRepresentation().absoluteString
        
        cell.entityImage.image = AsyncImageView.sharedLoader.getImage(moID.replace("/", withString: "") + currentAuthor.name)
        
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            
            if(searchActive){
                
                let author = filtered[indexPath.row]
                filtered.removeAtIndex(indexPath.row)
                author.delete()
               
            }else {
                
                let author = authorDataSource[indexPath.row]

                let alertController = UIAlertController(title: "Alerta", message: "Desea eliminar al autor " + author.name, preferredStyle: .Alert)
                
                let OKAction = UIAlertAction(title: "SI", style: .Default) { (action) in
                    self.authorDataSource.removeAtIndex(indexPath.row)
                    author.delete()
                    self.tableView.reloadData()
                }
                alertController.addAction(OKAction)
                
                let DeleteAction = UIAlertAction(title: "NO", style: .Default) { (action) in
                    self.tableView.reloadData()
                }
                alertController.addAction(DeleteAction)
                
                self.presentViewController(alertController, animated: true) {
                    // ...
                }
            }
            
            tableView.reloadData()
            
        }
    }
    
}

extension ViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if(searchActive){
            currentAuthor = filtered[indexPath.row]
        }else {
            currentAuthor = authorDataSource[indexPath.row]
        }
        
        toEventDetailsViewController()
    }
    
    func toEventDetailsViewController() {
        
        dispatch_async(dispatch_get_main_queue()) {
            // update some UI
            self.performSegueWithIdentifier("toCommentInteraction", sender: nil)
        }
    }
}

extension ViewController {
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "toPeopleInteraction" {
            let eventPeopleInteractionViewController = segue.destinationViewController as! NewAuthorVC
            eventPeopleInteractionViewController.interactionType = .saveInteraction
            
        }else {
            
            let eventPeopleInteractionViewController = segue.destinationViewController as! DetailAuthor
            eventPeopleInteractionViewController.currentAuthor = currentAuthor
        }
    }
}

