//
//  Author.swift
//  DemoPrueba
//
//  Created by Procesos on 8/09/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit
import CoreData

class Author: AbstractEntity {
    
    @NSManaged var name:String
    @NSManaged var urlImagen:String
    
    override class func entityDescription() -> (NSEntityDescription){
        return DataManager.getEntity("Author")
    }
    
    class func saveAuthor(name: String, urlImagen: String, image: UIImage) {
        
        let ctxAuthor:Author = Author(entity: self.entityDescription(), insertIntoManagedObjectContext: nil)
        ctxAuthor.name = name
        ctxAuthor.urlImagen = urlImagen
        ctxAuthor.save()
        
        //LOGICA PARA GUARDAR IMAGENES AUNQUE EL NOMBRE SE REPITA
        
        let moID = ctxAuthor .objectID.URIRepresentation().absoluteString
        ctxAuthor.urlImagen = moID.replace("/", withString: "") + name
        ctxAuthor.save()
        
        if let data = UIImagePNGRepresentation(image) {
            AsyncImageView.sharedLoader.saveImageDocumentDirectory(ctxAuthor.urlImagen, imageData: data)
        }
        
    }
    
}

extension String
{
    func replace(target: String, withString: String) -> String
    {
        return self.stringByReplacingOccurrencesOfString(target, withString: withString, options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
}