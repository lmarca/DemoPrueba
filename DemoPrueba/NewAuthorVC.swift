//
//  NewAuthorVC.swift
//  DemoPrueba
//
//  Created by Luis on 8/09/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

enum EventsPeopleInteractionType {
    case saveInteraction
    case editInteraction
}

class NewAuthorVC: UIViewController, UINavigationControllerDelegate {
    
    var currentAuthor : Author!
    var takenImage: UIImage?
    var IDUniqueAuthor: String?
    var interactionType: EventsPeopleInteractionType!
    
    @IBOutlet weak var profileAuthor: ProfileImageView!
    
    @IBOutlet weak var txtNameAuthor: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(NewAuthorVC.choosePhotoSource))
        profileAuthor.addGestureRecognizer(tapGestureRecognizer)
        
        if interactionType == .saveInteraction {
            self.navigationItem.title = "Nuevo Autor"
        }
        else {
            self.navigationItem.title = "Editar Autor"
            
            IDUniqueAuthor = currentAuthor .objectID.URIRepresentation().absoluteString.replace("/", withString: "")
            
            txtNameAuthor.text = currentAuthor.name
            
            if currentAuthor.urlImagen.characters.count > 0 {
                profileAuthor.image = AsyncImageView.sharedLoader.getImage(IDUniqueAuthor! + currentAuthor.name)
            }
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickInput(sender: AnyObject) {
        userAction()
    }

    private func userAction() {
 
        if txtNameAuthor.text!.isEmpty {
            
            let alertController = UIAlertController(title: "Demo", message: "Ingrese un nombre", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
            
        }else {
            if interactionType == .editInteraction {
                if txtNameAuthor.text?.characters.count > 0 {
                    currentAuthor.name = txtNameAuthor.text!
                    
                    if let data = UIImagePNGRepresentation(profileAuthor.image!) {
                        AsyncImageView.sharedLoader.saveImageDocumentDirectory(IDUniqueAuthor! + currentAuthor.name, imageData: data)
                    }
                    
                    currentAuthor.save()
                    
                }
            }else {
                Author.saveAuthor(txtNameAuthor.text!, urlImagen: "", image: profileAuthor.image!)
            }
            
            showAlert()
        }
    }
    
    private func showAlert() {
        let alertController = UIAlertController(title: "Alerta", message: interactionType == .saveInteraction ? "Registro exitoso" : "Modificacion exitosa", preferredStyle: .Alert)
   
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            
            self.navigationController?.popViewControllerAnimated(true)
            
        }
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true) {
            // ...
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//MARK: Photo Image
extension NewAuthorVC {
    
    func choosePhotoSource() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let chooseFromLibrary = UIAlertAction(title: "Escoger de la fototeca", style: .Default)  { (action) in
            
            NSOperationQueue.mainQueue().addOperationWithBlock({
                self.initializeImagePickerController(.PhotoLibrary)
            })
        }
        
        alertController.addAction(chooseFromLibrary)
        
        
        let takePhoto = UIAlertAction(title: "Tomar con la cámara", style: .Default)  { (action) in
            
            NSOperationQueue.mainQueue().addOperationWithBlock({
                self.initializeImagePickerController(.Camera)
            })
        }
        
        alertController.addAction(takePhoto)
        
        let cancel = UIAlertAction(title: "Cancelar", style: .Cancel, handler:nil)
        alertController.addAction(cancel)
        
        self.presentViewController(alertController, animated:true, completion: nil)
    }
    
    func initializeImagePickerController(sourceType: UIImagePickerControllerSourceType) {
        
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        picker.sourceType = sourceType
        presentViewController(picker, animated: true, completion: nil)
    }
}

extension NewAuthorVC: UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        var newImage: UIImage
        
        if let possibleImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            newImage = possibleImage
        }
            
        else if let possibleImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            newImage = possibleImage
        }
            
        else {
            return
        }
        
        takenImage = newImage
        profileAuthor.image = takenImage
        
        dismissViewControllerAnimated(true, completion: nil)
    }
}

extension NewAuthorVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /*
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        userAction()
        return true
    }*/
}
