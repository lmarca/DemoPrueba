//
//  Libro.swift
//  DemoPrueba
//
//  Created by Luis on 13/09/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit
import CoreData

class Libro: AbstractEntity {
    
    @NSManaged var idauthor:String
    @NSManaged var name:String
    @NSManaged var urlImagen:String
    @NSManaged var year:String
    
    override class func entityDescription() -> (NSEntityDescription){
        return DataManager.getEntity("Libro")
    }
    
    class func saveLibro(idauthor: String, name: String, urlImagen: String, year: String, image: UIImage) {
        
        let ctxLibro:Libro = Libro(entity: self.entityDescription(), insertIntoManagedObjectContext: nil)
        ctxLibro.idauthor = idauthor
        ctxLibro.name = name
        ctxLibro.urlImagen = urlImagen
        ctxLibro.year = year
        ctxLibro.save()
        
        if urlImagen.isEmpty {
            let moID = ctxLibro .objectID.URIRepresentation().absoluteString
            if let data = UIImagePNGRepresentation(image) {
                AsyncImageView.sharedLoader.saveImageDocumentDirectory(moID.replace("/", withString: "") + name, imageData: data)
            }
        }
    
    }

}
