
import Foundation
import CoreData

class AbstractEntity: NSManagedObject{
    
    override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext?) {
        super.init(entity: entity, insertIntoManagedObjectContext: nil)
    }
    
    class func entityDescription() -> (NSEntityDescription) {
        fatalError("Must Override")
    }
    
    func delete(){
        
        let context:NSManagedObjectContext = DataManager.getContext()
        var error:NSError?
        
        context.deleteObject(self)
        do {
            try context.save()
        } catch let error1 as NSError {
            error = error1
        }
        
        if (error != nil){
            NSLog(error!.description)
        }
    }
    
    func save(){
        
        let context:NSManagedObjectContext = DataManager.getContext()
        var error:NSError?
        
        if (self.managedObjectContext == nil) {
            context.insertObject(self)
        }
        
        do {
            try context.save()
        } catch let error1 as NSError {
            error = error1
        }
        
        if (error != nil){
            NSLog(error!.description)
        }
    }
    
}