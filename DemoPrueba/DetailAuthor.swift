//
//  DetailAuthor.swift
//  DemoPrueba
//
//  Created by Luis on 13/09/16.
//  Copyright © 2016 Luis Marca Zapata. All rights reserved.
//

import UIKit

class DetailAuthor: UIViewController {
    
    var currentAuthor : Author!
    var IDUniqueAuthor: String?
    var libroDataSource = [Libro]()
    var currentLibro : Libro!
    
    @IBOutlet weak var profileAuthor: ProfileImageView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(animated: Bool) {
        
        self.navigationItem.title = currentAuthor.name
        
        if currentAuthor.urlImagen.characters.count > 0 {
            profileAuthor.image = AsyncImageView.sharedLoader.getImage(IDUniqueAuthor! + currentAuthor.name)
        }

        let predicate = NSPredicate(format: "idauthor == %@", IDUniqueAuthor!)
        
        let result = DataManager.getManagedObjectsFromEntity(Libro.entityDescription(), predicate: predicate)
        if (result.sucess){
            if result.objects.count > 0 {
                libroDataSource.removeAll()
                for libro in result.objects as! [Libro]{
                    if !libroDataSource.contains(libro) {
                        libroDataSource .append(libro)
                    }
                }
            }
        }
        
        tableView.reloadData()
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = true
        self.edgesForExtendedLayout = .None
        
        IDUniqueAuthor = currentAuthor .objectID.URIRepresentation().absoluteString.replace("/", withString: "")
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickInput(sender: AnyObject) {
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension DetailAuthor: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return libroDataSource.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        var numOfSections: Int = 0
        if libroDataSource.count > 0
        {
            tableView.separatorStyle = .SingleLine
            numOfSections                = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height))
            noDataLabel.text             = "No data available"
            noDataLabel.textColor        = UIColor.blackColor()
            noDataLabel.textAlignment    = .Center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .None
        }
        return numOfSections
        //return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CustomCell", forIndexPath: indexPath) as! CustomCell

        currentLibro = libroDataSource[indexPath.row]
        
        cell.entityName.text = currentLibro.name
        
        if currentLibro.urlImagen.characters.count > 0 {
            cell.entityImage.sd_setImageWithURL(NSURL(string: currentLibro.urlImagen), placeholderImage: UIImage(named: "defaultProfile"))
        }else {
            cell.entityImage.image = AsyncImageView.sharedLoader.getImage(currentLibro .objectID.URIRepresentation().absoluteString.replace("/", withString: "") + currentLibro.name)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // handle delete (by removing the data from your array and updating the tableview)
        
            let libro = libroDataSource[indexPath.row]
            
            let alertController = UIAlertController(title: "Alerta", message: "Desea eliminar el libro " + libro.name, preferredStyle: .Alert)
            
            let OKAction = UIAlertAction(title: "SI", style: .Default) { (action) in
                self.libroDataSource.removeAtIndex(indexPath.row)
                libro.delete()
                self.tableView.reloadData()
            }
            alertController.addAction(OKAction)
            
            let DeleteAction = UIAlertAction(title: "NO", style: .Default) { (action) in
                self.tableView.reloadData()
            }
            alertController.addAction(DeleteAction)
            
            self.presentViewController(alertController, animated: true) {
                // ...
            }
        }
    }
    
}

extension DetailAuthor: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        currentLibro = libroDataSource[indexPath.row]
        toEventDetailsViewController()
    }
    
    func toEventDetailsViewController() {
        
        dispatch_async(dispatch_get_main_queue()) {
            // update some UI
            self.performSegueWithIdentifier("toEditInteraction", sender: nil)
        }
    }
}

extension DetailAuthor {
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "toRegisterInteraction" {
            
            let eventPeopleInteractionViewController = segue.destinationViewController as! NewLibro
            eventPeopleInteractionViewController.interactionType = .saveInteraction
            eventPeopleInteractionViewController.currentAuthor = currentAuthor
        }
            
        if segue.identifier == "toEditInteraction" {
            
            let eventPeopleInteractionViewController = segue.destinationViewController as! NewLibro
            eventPeopleInteractionViewController.interactionType = .editInteraction
            eventPeopleInteractionViewController.currentLibro = currentLibro
            eventPeopleInteractionViewController.currentAuthor = currentAuthor
            
        }
        
        if segue.identifier == "toCommentInteraction" {
            
            let eventPeopleInteractionViewController = segue.destinationViewController as! NewAuthorVC
            eventPeopleInteractionViewController.interactionType = .editInteraction
            eventPeopleInteractionViewController.currentAuthor = currentAuthor
            
        }
    }
}